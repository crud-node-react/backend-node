const express = require("express");
const router = express.Router();
const registrationCtrl = require('./controller/registration');

/**
 * @swagger
 *  components:
 *      schema:
 *          Registration:
 *              type: object
 *              properties:
 *                  id:
 *                      type: string
 *                  name:
 *                      type: string
 *                  mobile:
 *                      type: string
 */

/**
 * @swagger
 * /registration/insertdata:
 *   post:
 *     description: api to insert data
 *     requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema: 
 *                      $ref: '#components/schema/Registration'
 *     responses:
 *         200:
 *             description: Success
 */
router.post('/registration/insertdata', async function (req, res) {
    var response = await registrationCtrl.add(req, res);
    return response;
});

/**
 * @swagger
 * /registration/getdata:
 *   get:
 *     description: Code for fetch data from database
 *     responses:
 *       200:
 *         description: Success
 */
router.get('/registration/getdata', async function (req, res) {
    var response = await registrationCtrl.fetch(req, res);
    return response;
});

/**
 * @swagger
 * /registration/updatedata:
 *   put:
 *     description: api to update data
 *     requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema: 
 *                      $ref: '#components/schema/Registration'
 *     responses:
 *         200:
 *             description: Success
 */
router.put('/registration/updatedata', async function (req, res) {
    var response = await registrationCtrl.edit(req, res);
    return response;
});

/**
 * @swagger
 * /registration/deletedata/{id}:
 *   delete:
 *     description: Code delete data from database
 *     parameters:
 *          - in: path
 *            name: id
 *            required: true
 *            schema:
 *              type: string
 *     responses:
 *       200:
 *         description: Data is deleted
 */
router.delete('/registration/deletedata/:id', async function (req, res) {
    var response = await registrationCtrl.remove(req, res);
    return response;
});

module.exports = router;