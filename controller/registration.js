const Registration = require('../models/registration');
const registrationObj = new Registration();


class RegistrationCtrl {
    /**
     * Code for Insert data into database
     * last modified: 03/08/2021
     */
    async add(req, res) {
        console.log('inside controller add()');
        /**
         * request validation
         */
        var response = await registrationObj.addData(req, res);
        return response;
    };

    /**
     * Code for fetch data from database
     * last modified: 03/08/2021
     */
    async fetch(req, res) {
        console.log('inside controller fetch()');

        /**
         * request validation
         */

        var response = await registrationObj.fetchData(req, res);
        return response;
    };

    /**
     * Code for Update data of database
     * last modified: 03/08/2021
     */
    async edit(req, res) {
        console.log('inside controller edit()');

        /**
         * request validation
         */

        var response = await registrationObj.updateData(req, res);
        return response;
    };

    /**
     * Code to Delete data from database
     * last modified: 03/08/2021
     */
    async remove(req, res) {
        console.log('inside controller remove()');

        /**
         * request validation
         */

        var response = await registrationObj.deleteData(req, res);
        return response;
    };
}
module.exports = new RegistrationCtrl();