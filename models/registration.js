const registration = require("../utility/dbmodels/registration");
const dbconfig = require('../config/dbconfig');
const constants = require('../utility/constants/messages');
// const mongoose = require("mongoose");

class Registration {

    // constructor() {
    //     openDbConnection();
    // }

    // openDbConnection() {
    //     // mongoose connection
    //     mongoose.connect(process.env.mongourl, { useUnifiedTopology: true, useNewUrlParser: true });
    //     const connection = mongoose.connection;
    //     connection.once("open", function () {
    //         console.log("MongoDB connected");
    //     });
    // }

    /**
     * Code for Insert data into database
     * last modified: 03/08/2021
     */
    addData(req, res) {
        console.log('inside model addData()');
        try {
            let dataObj = { name: req.body.name, mobile: req.body.mobile };
            registration.insertMany(dataObj, function (err, result) {
                if (err) {
                    console.log('error in  insert data', err);
                    res.status(constants.STATUS_CODE.INTERNAL_SERVER_ERROR).send(err);
                } else {
                    res.status(constants.STATUS_CODE.SUCCESS).send(result);
                }
            });
        } catch (err) {
            console.log('error in insert data', err);
            throw err;
        }
    }

    /**
     * Code for fetch data from database
     * last modified: 03/08/2021
     */
    fetchData = (req, res) => {
        console.log('inside model fetchData()');
        try {
            registration.find(function (err, result) {
                if (err) {
                    console.log('error in getdata', err);
                    res.status(constants.STATUS_CODE.INTERNAL_SERVER_ERROR).send(err);
                } else {
                    res.status(constants.STATUS_CODE.SUCCESS).send(result);
                }
            });
        } catch (err) {
            console.log('error in getdata', err);
            throw err;
        }
    }

    /**
     * Code for Update data of database
     * last modified: 03/08/2021
     */
    updateData = (req, res) => {
        console.log('inside model updateData()');
        try {
            let updateObj = { name: req.body.name, mobile: req.body.mobile }
            registration.findByIdAndUpdate(req.body.id, updateObj, { new: true }, function (err, result) {
                if (err) {
                    console.log('error in updatedata', err);
                    res.status(constants.STATUS_CODE.INTERNAL_SERVER_ERROR).send(err);
                } else {
                    res.status(constants.STATUS_CODE.SUCCESS).send(result);
                }
            });
        }
        catch (err) {
            console.log('error in updatedata', err);
            throw err;
        }
    }

    /**
     * Code to Delete data from database
     * last modified: 03/08/2021
     */
    deleteData = (req, res) => {
        console.log('inside model deleteData()');
        try {
            registration.remove({ _id: req.params.id }, function (err, result) {
                if (err) {
                    console.log('error in deletedata', err);
                    res.status(constants.STATUS_CODE.INTERNAL_SERVER_ERROR).send(err);
                } else {
                    res.status(constants.STATUS_CODE.SUCCESS).send(result);
                }
            });
        }
        catch (err) {
            console.log('error in deletedata', err);
            throw err;
        }
    }
    // router.post('/update', function(req, res) {
    //     registration.findByIdAndUpdate(req.body.id, 
    //     {Name:req.body.Name}, function(err, data) {
    //         if(err){
    //             console.log(err);
    //         }
    //         else{
    //             res.send(data);
    //             console.log("Data updated!");
    //         }
    //     });  
    // });

}

module.exports = Registration;