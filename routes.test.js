const { TestWatcher } = require('jest');
const request = require('supertest');
const app = require('./server');

// afterAll((done) => {
//     app.close(() => {
//       done();
//     });
//   });

// beforeEach(async () => {
//     await model.deleteMany({})
// });

test('should insert new record', async () => {
    await request(app).post('/registration/insertdata')
        .send({
            name: 'test',
            mobile: '9090009909'
        }).expect(200)
})

test("should retrive records", async () => {
    await request(app).get("/registration/getdata")
        .expect(200)
        .then((response) => {
            expect(Array.isArray(response.body)).toBeTruthy();
            expect(response.body.length).toBeGreaterThan(0);
        });
});

test("should delete records", async () => {
    const testRecords = await request(app)
        .post("/registration/insertdata")
        .send({
            name: "testJest",
            mobile: "9090009909"
        });
    const removedData = await request(app).delete(
        `/registration/deletedata/${testRecords.body[0]._id}`
    );
    // expect(removedData.body).toBe({ message: "Deleted" });
    expect(removedData.body.deletedCount).toBeGreaterThan(0);
    expect(removedData.statusCode).toBe(200);
});

test("should update records", async () => {
    const testData = await request(app)
        .post("/registration/insertdata")
        .send({
            name: "test update",
            mobile: "9090009909"
        });

    const updatedData = await request(app)
        .put(`/registration/updatedata`)
        .send({ id: testData.body[0]._id, name: "updated", mobile: "8080818180" });

    expect(updatedData.body.name).toBe("updated");
    expect(updatedData.body).toHaveProperty("_id");
    expect(updatedData.statusCode).toBe(200);

});