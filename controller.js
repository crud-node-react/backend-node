const express = require("express");
const router = express.Router();
const registration = require("./model");


/**
 * @swagger
 * /insertdata:
 *   post:
 *     summary: Code for fetch data from database
 *     consumes:
 *          - application/json
 *     parameters:
 *        - in: body
 *          name: user
 *          mobile: user
 *          description: The user to create.
 *          schema:
 *              $ref: '#\utility\dbmodels\registration.js'
 *   responses:
 *       200:
 *         description: Success
 * definitions:
 *   Registration:
 *      type: object
 *      required:
 *          - name
 *      properties:
 *          name:
 *              type: string
 *          mobile:
 *              type: string
 */
/**
 * Code for Insert data into database
 */
router.post('/insertdata', function (req, res) {
    try {
        console.log(req.body)
        registration.insertMany(req.body.user, function (err, result) {
            if (err) {
                console.log('error in insert data', err);
                res.send(err);
            } else {
                res.status(201).send(result);
            }
        });
    } catch (err) {
        console.log('error in insert data', err);
        throw err;
    }
});

/**
 * @swagger
 * /getdata:
 *   get:
 *     description: Code for fetch data from database
 *     responses:
 *       200:
 *         description: Success
 */
/**
 * Code for fetch data from database
 */
router.get('/getdata', function (req, res) {
    try {
        registration.find(function (err, result) {
            if (err) {
                console.log('error in getdata', err);
                res.send(err);
            } else {
                res.status(200).send(result);
            }
        });
    } catch (err) {
        console.log('error in getdata', err);
        throw err;
    }
});

/**
 * Code for Update data of database
 */
router.put('/updatedata', function (req, res) {
    try {
        updateObj = { name: req.body.name, mobile: req.body.mobile }
        registration.findByIdAndUpdate(req.body.id, updateObj, { new: true }, function (err, result) {
            if (err) {
                console.log('error in updatedata', err);
                res.send(err);
            } else {
                res.status(200).send(result);
            }
        });
    }
    catch (err) {
        console.log('error in updatedata', err);
        throw err;
    }
});

/**
 * Code to Delete data from database
 */
router.delete('/deletedata/:id', function (req, res) {
    try {
        registration.remove({ _id: req.params.id }, function (err, result) {
            if (err) {
                console.log('error in deletedata', err);
                res.send(err);
            } else {
                res.status(200).send(result);
            }
        });
    }
    catch (err) {
        console.log('error in deletedata', err);
        throw err;
    }
});
// router.post('/update', function(req, res) {
//     registration.findByIdAndUpdate(req.body.id, 
//     {Name:req.body.Name}, function(err, data) {
//         if(err){
//             console.log(err);
//         }
//         else{
//             res.send(data);
//             console.log("Data updated!");
//         }
//     });  
// });



module.exports = router;