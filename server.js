const express = require("express");
const app = express();
const mongoose = require("mongoose");
// const dbconfig = require('./config/dbconfig');
const controller = require('./controller');
const routes = require('./routes');
const cors = require("cors");
require('dotenv').config();

app.use(cors());

/**
 * parse requests of content-type - application/json
 */
app.use(express.json());

/**
 * parse requests of content-type - application/x-www-form-urlencoded
 */
app.use(express.urlencoded({ extended: true }));

/**
 * Mongoose connection
 */
mongoose.connect(process.env.mongourl, { useUnifiedTopology: true, useNewUrlParser: true });
const connection = mongoose.connection;
connection.once("open", function () {
    console.log("MongoDB connected");
});

/**
 * Swagger implementation 
 */
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const swaggerOptions = {
    swaggerDefinition: {
        openapi: '3.0.0',
        info: {
            tilte: 'Library API',
            version: '1.0.0'
        }
    },
    apis: ['routes.js'],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
// console.log(swaggerDocs);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));


// app.use("/", controller);
app.use("/", routes);

app.listen(process.env.port, function () {
    console.log("Server is running on Port: " + process.env.port);
});

module.exports = app;