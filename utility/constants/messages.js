const messages = {
    DB_MESSAGE: {
        DATABASE_ERROR: 'DB connection error',
        NO_RECORD_FOUND: 'No record found in database'
    },
    REQUEST_VALIDATION: {
        NAME_IS_REQUIRED: 'Name is required'
    },
    STATUS_CODE: {
        SUCCESS: 200,
        NOT_FOUND: 404,
        INTERNAL_SERVER_ERROR: 500,
        BAD_REQUEST: 400,
        UNAUTHORIZED: 401
    }
}

module.exports = messages;