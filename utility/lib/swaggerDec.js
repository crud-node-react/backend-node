
/**
 * swagger implementation 
 */
 const swaggerJsDoc = require('swagger-jsdoc');
 const swaggerUi = require('swagger-ui-express');
 
 const swaggerOptions = {
     swaggerDefinition: {
         openapi: '3.0.0',
         info: {
             tilte: 'Library API',
             version: '1.0.0'
         }
     },
     apis: ['../../routes.js'],
 };
 
 const swaggerDocs = swaggerJsDoc(swaggerOptions);
 // console.log(swaggerDocs);
 app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));